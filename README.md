## MakanApa Workout Worker Service

### Requirement:
- Python 3.9.5
- Pipenv
- RabbitMQ

### To use:
1. clone this repository
2. run `pipenv shell`
3. isntall dependencies with `pipenv install`
4. Turn on your existing RabbitMQ and enable web socket STOMP
5. to run the API `python manage.py runserver`
6. to activate worker to send data to message queue `python manage.py process_tasks`