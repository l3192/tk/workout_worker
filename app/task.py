import json

import pika as pika
from background_task import background
from pika import exceptions


def define_rabbitmq_connection():
    global connection
    global channel

    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange='user', exchange_type='topic')


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
define_rabbitmq_connection()



@background
def change_item(item, iteration, user_id):
    global connection
    global channel

    if connection.is_closed:
        define_rabbitmq_connection()

    item['time'] -= iteration

    declared_queue = channel.queue_declare(queue=f'{user_id}')
    channel.queue_bind(exchange='user', queue=declared_queue.method.queue)

    try:
        channel.basic_publish(exchange='user', routing_key=f'{user_id}', body=bytes(json.dumps(item), encoding="UTF-8"))
    except (exceptions.ConnectionClosed, exceptions.ChannelClosed):
        define_rabbitmq_connection()
