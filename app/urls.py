from django.urls import path, include

from rest_framework.routers import SimpleRouter

from app.views import AppViewSet

router = SimpleRouter(trailing_slash=False)
router.register('workout-worker', AppViewSet, basename='workout-worker')

urlpatterns = [
    path('', include(router.urls)),
]
