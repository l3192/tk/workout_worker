from rest_framework import fields
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.viewsets import ViewSet

from app.task import change_item


class WorkoutItemSerializer(Serializer):
    name = fields.CharField(required=True)
    time = fields.IntegerField(required=True)
    estimatedCalories = fields.IntegerField(required=True)


class WorkoutDataRequest(Serializer):
    userId = fields.CharField(required=True)
    workouts = fields.ListField(child=WorkoutItemSerializer())


class AppViewSet(ViewSet):
    @action(methods=['post'], detail=False, url_path='receive-workout-data')
    def add_workout(self, request: Request):
        serializer = WorkoutDataRequest(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        total_time = 0
        for item in data['workouts']:
            iteration = 0
            for time in range(item['time']):
                total_time += 1
                iteration += 1

                change_item(item, iteration, data['userId'], schedule=total_time)

        return Response(data={'success'})
